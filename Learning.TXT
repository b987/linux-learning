
GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~
$ mkdir DevOps

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~
$ cd Devops

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ls

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ls-lrt
bash: ls-lrt: command not found

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ssh-keygen -f devops
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in devops
Your public key has been saved in devops.pub
The key fingerprint is:
SHA256:HnWi8ZVlQDKQkh6DCM6jEbyauj1WkpOGPbiLzB+J9Yk GAINWELL+ygoduguchint@GWT-5CG016FY8T
The key's randomart image is:
+---[RSA 3072]----+
|+. . . ..oo.o.o  |
|oo. . = .  o +   |
|.+.  . +. o +    |
|.o.   .  = +     |
|o= +    S .      |
|= @ = .. .       |
|.+ E o  .        |
|*.o .            |
|+*oo             |
+----[SHA256]-----+

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ls
devops  devops.pub

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ mv devops devops.pem

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ls
devops.pem  devops.pub

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ls
devops.pem  devops.pub

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ cat devops.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC89PFt3Tlw56qT2isZA+h+LSuOpq/JcPwDkfw6UKB5lhJdEslMcN3NupI4AAIYIr25t5Mp90a6uVopK1xcS96rYwtE+0TBHryMPUF+8jwhBywls76+9Zo7IO9ndYeXr6pC7YF12LLfDMCElML5n+WGh2s0ikBTJSZ13k0mcerX5WDZdsUdY1oyTBYF7cxET6yHh9bqU29pzeFKvooc9rP+xY80hLh0CUec3VYpxh8MNhcjwhtx89xMjacur6L+jUboZ0Ug6Ndy2vNxgw8TDLc2MhLxd5v00D2xWuLb47hM+7opZOZR1G20r6AhIpKvEqafgiRf6ZJDmnByOJuLrrEUoGQAc4qkFRbOYgai/3L8GKoQVzKaw4KXqGieahG//SgITpcNO/KgXW7AYVi93WymCcwl68vKmI89ycBOXpA1epS0mFPs82HMVMJQHUDGoxd++hgUpU4sMdXd6yxMvhQNMLyRK33RsaodkgzTAoWU23RVs2Or4FHX1jGOojJLSNE= GAINWELL+ygoduguchint@GWT-5CG016FY8T

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ^C

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ pwd
/c/Users/ygoduguchint/Devops

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ^C

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ls -lrt
total 5
-rw-r--r-- 1 GAINWELL+ygoduguchint 4096 2635 Sep 28 11:33 devops.pem
-rw-r--r-- 1 GAINWELL+ygoduguchint 4096  590 Sep 28 11:33 devops.pub

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ssh -i devops.pem centos @15.206.153.162
ssh: Could not resolve hostname centos: Name or service not known

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ssh -i  devops.pem centos@15.206.153.162
The authenticity of host '15.206.153.162 (15.206.153.162)' can't be established.
ED25519 key fingerprint is SHA256:zChOCENloucaYaoYjVb+oFfhorWC7cfXZ0d2rBItz0Q.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])?
Host key verification failed.

GAINWELL+ygoduguchint@GWT-5CG016FY8T MINGW64 ~/Devops
$ ssh -i  devops.pem centos@15.206.153.162
The authenticity of host '15.206.153.162 (15.206.153.162)' can't be established.
ED25519 key fingerprint is SHA256:zChOCENloucaYaoYjVb+oFfhorWC7cfXZ0d2rBItz0Q.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? Yes
Warning: Permanently added '15.206.153.162' (ED25519) to the list of known hosts.
[centos@ip-172-31-41-40 ~]$ uptime
 08:05:01 up 16 min,  1 user,  load average: 0.00, 0.01, 0.04
[centos@ip-172-31-41-40 ~]$ exit
logout
Connection to 15.206.153.162 closed.
